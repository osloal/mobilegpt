import { defineStore } from "pinia";

export const userStatusStore = defineStore("userStatus", {
  state: () => {
    return {
      isLogin:false,
      token: "",
      userNickName:'',
      bottomTabIndex:0,
      isShowBottomIndex:true,
    };
  },
  persist: true
});