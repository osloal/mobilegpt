import request from './request'

const http = {
    get(url,params){
        const config = {
            method:'GET',
            url:url,
            params:params ? params :{},
        }
        return request(config)
    },
    post(url,params,headers){
        const config = {
        method:'POST',
            url:url,
            data:params ? params :{},
            headers:{
                'Content-Type':headers ? headers : 'application/json'
            }
        }
        return request(config)
    }
}
export default http