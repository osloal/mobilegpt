import axios from 'axios'
import { showToast } from 'vant';

const service = axios.create({
    baseURL:import.meta.env.VITE_BASE_API_URL,
    timeout:10*1000,
})

const getToken = () =>{
    const userStatus = JSON.parse(localStorage.getItem('userStatus') || '{}')
    // return userStatus.token
    return 'wXsiGh04IlTxL4W5i6TRM3Opz9JZH9cmPepl5yGPcTegdSM85MYiJLtwIQuB'
}

service.interceptors.request.use(config => {
    config.headers.Authorization ='Bearer ' +  getToken() || '';
    return config
})

service.interceptors.response.use(response =>{
    if(response.status == 200){
        showToast({
                message:response.data.message,
                position: 'top',
            }
        )
        return response.data
    }else{
        showToast({
                message:response.data.message,
                position: 'top',
            }
        )
    }
},error => {
    showToast(
        {
            message:error.message,
            position: 'top',
        }
    )
})

export default service