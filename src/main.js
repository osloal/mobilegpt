import '@/tool/style/animate.less';
import '@/tool/style/global.less';
import { createApp } from 'vue'
import { createPinia } from 'pinia'

import App from './App.vue'
import router from './router'

import Vant from 'vant';
import 'vant/lib/index.css';

import 'amfe-flexible'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'

const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);

const app = createApp(App)
app.use(pinia)
app.use(router)
app.use(Vant);
app.mount('#app')
