import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'main',
      component: () => import('../views/Main.vue'),
      meta:{
        index:0,
        keepAlive:true,
        showBottomTab:true
      },
      redirect:'/',
      children:[
        {
          path: '/',
          name: 'home',
          component: () => import('../views/Home/index.vue'),
          meta:{
            index:0,
            keepAlive:true,
            showBottomTab:true
          },
        },
        {
          path: '/draw',
          name: 'draw',
          component: () => import('../views/Draw/index.vue'),
          meta:{
            index:1,
            keepAlive:true,
            showBottomTab:true
          },
        },
        {
          path: '/user',
          name: 'user',
          component: () => import('../views/User/index.vue'),
          meta:{
            index:2,
            keepAlive:true,
            showBottomTab:true
          },
        },
        {
          path:'/chat',
          name:"chat",
          component: ()=> import('../views/Chat/index.vue'),
          meta:{
            index:3,
            keepAlive:false,
            showBottomTab:false
          }
        }
      ]
    }
    
  ]
})

export default router
